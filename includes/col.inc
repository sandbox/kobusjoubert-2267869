<?php

/**
 * @file
 * Replace a given text formatted like a span.
 *
 * @param $attrs
 * @param $text
 */
function shortcode_bootstrap_shortcode_col($attrs, $text) {
  $attrs = shortcode_attrs(array(
      'class' => '',
      'span' => '',
      'phone' => '',
      'tablet' => '',
      'desktop' => '',
      'wide' => '',
      'xs' => '',
      'sm' => '',
      'md' => '',
      'lg' => '',
    ),
    $attrs
  );

  $class = '';

  // Bootstrap 2 span class will fallback to the col-sm-n class.
  $class .= empty($attrs['span']) ? '' : 'col-sm-' . $attrs['span'] . ' ';

  // Bootstrap 3 human readable classes for clients.
  $class .= empty($attrs['phone']) ? '' : 'col-xs-' . $attrs['phone'] . ' ';
  $class .= empty($attrs['tablet']) ? '' : 'col-sm-' . $attrs['tablet'] . ' ';
  $class .= empty($attrs['desktop']) ? '' : 'col-md-' . $attrs['desktop'] . ' ';
  $class .= empty($attrs['wide']) ? '' : 'col-lg-' . $attrs['wide'] . ' ';

  // Bootstrap 3 col classes for developers.
  $class .= empty($attrs['xs']) ? '' : 'col-xs-' . $attrs['xs'] . ' ';
  $class .= empty($attrs['sm']) ? '' : 'col-sm-' . $attrs['sm'] . ' ';
  $class .= empty($attrs['md']) ? '' : 'col-md-' . $attrs['md'] . ' ';
  $class .= empty($attrs['lg']) ? '' : 'col-lg-' . $attrs['lg'] . ' ';

  $class = shortcode_add_class($attrs['class'], $class);

  return theme('shortcode_col', array('text' => $text, 'class' => $class));
}

/**
 * Helper function.
 *
 * @see shortcode_bootstrap_shortcode_col().
 *
 * @param $vars
 */
function theme_shortcode_col($vars) {
  $output = '<div class="' . $vars['class'] . '">' . $vars['text'] . '</div>';;

  return $output;
}

/**
 * Callback function for shortcode_bootstrap_shortcode_row.
 *
 * @see shortcode_bootstrap_shortcode_info().
 */
function shortcode_bootstrap_shortcode_col_tip($format, $long) {
  $output = array();
  $human_attributes = 'phone="1-12"|tablet="1-12"|desktop="1-12"|wide="1-12"';
  $developer_attributes = 'xs="1-12"|sm="1-12"|md="1-12"|lg="1-12"';
  $deprecated_attributes = 'span="1-12"';
  $attributes = $human_attributes . '|' . $developer_attributes . '|' . $deprecated_attributes;
  $output[] = '<p><strong>' . t('[col (' . $attributes . '|class="additional class")]text[/col]') . '</strong> ';

  if ($long) {
    $output[] = t('Wraps the text in a div with a col class.') . '</p>';
  } 
  else {
    $output[] = t('Wraps the text in a div with a col class. Additional class names can be added by the <em>class</em> parameter.') . '</p>';
  }

  return implode(' ', $output);
}
