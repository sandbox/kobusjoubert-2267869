<?php

/**
 * @file
 * Replace a given text formatted like a row.
 *
 * @param $attrs
 * @param $text
 */
function shortcode_bootstrap_shortcode_row($attrs, $text) {
  $attrs = shortcode_attrs(array(
      'class' => '',
    ),
    $attrs
  );

  $class = shortcode_add_class($attrs['class'], 'row');

  return theme('shortcode_row', array('text' => $text, 'class' => $class));
}

/**
 * Helper function.
 *
 * @see shortcode_bootstrap_shortcode_row().
 *
 * @param $vars
 */
function theme_shortcode_row($vars) {
  $output = '<div class="' . $vars['class'] . '">' . $vars['text'] . '</div>';

  return $output;
}

/**
 * Callback function for shortcode_bootstrap_shortcode_row.
 *
 * @see shortcode_bootstrap_shortcode_info().
 */
function shortcode_bootstrap_shortcode_row_tip($format, $long) {
  $output = array();
  $output[] = '<p><strong>' . t('[row (class="additional class")]text[/row]') . '</strong> ';

  if ($long) {
    $output[] = t('Wraps the text in a div with a row class.') . '</p>';
  } 
  else {
    $output[] = t('Wraps the text in a div with a row class. Additional class names can be added by the <em>class</em> parameter.') . '</p>';
  }

  return implode(' ', $output);
}
